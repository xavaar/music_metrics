import os
import re
import unicodedata

import pandas as pd
import requests
from dotenv import load_dotenv
from lyricsgenius import Genius

load_dotenv()

# Retrieve the Genius access token
genius_access_token = os.getenv("GENIUS_ACCESS_TOKEN")

genius = Genius(genius_access_token)


def remove_invisible_characters(text):
    # Remove any invisible or non-printable characters
    return re.sub(r"[^\x00-\x7F]+", "", text)


def normalize_text(text):
    # Normalize and remove invisible characters
    text = unicodedata.normalize("NFKC", text)
    return remove_invisible_characters(text)


def find_exact_match(search_term, artist_hits):
    search_term = normalize_text(search_term)  # Normalize and clean search term

    for hit in artist_hits:
        artist_name = normalize_text(hit["result"]["name"])  # Normalize and clean artist name
        # print(f"SEARCH TERM: {search_term}\nARTIST NAME: {artist_name}\n")
        if artist_name == search_term:
            return hit["result"]  # Return the exact match

    return None


def find_genius_artist_id(artist_name: str):
    artist_hits = genius.search_artists(artist_name)["sections"][0]["hits"]
    exact_match = find_exact_match(artist_name, artist_hits)

    if exact_match:
        return exact_match["id"]
        # print(f"Exact match found: {exact_match['name']} (ID: {exact_match['id']})")
    else:
        return None


def find_genius_artist_info(artist_name: str = None, artist_id: int = None):
    if artist_id:
        artist_info = genius.artist(artist_id=artist_id, text_format=None)
    else:
        artist_id = find_genius_artist_id(artist_name=artist_name)
        artist_info = genius.artist(artist_id=artist_id, text_format=None)

    return artist_info


def find_genius_artist_info_for_langchain(artist_id: int):
    results = find_genius_artist_info(artist_id=artist_id)["artist"]

    return {
        key: results[key] for key in ["alternate_names", "description", "url"] if key in results
    }
