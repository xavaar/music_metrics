import os
import sys
from pathlib import Path

import pandas as pd
from dotenv import load_dotenv
from sqlalchemy import (
    Column,
    DateTime,
    Float,
    Integer,
    MetaData,
    String,
    Table,
    create_engine,
    inspect,
    text,
)
from sqlalchemy.orm import sessionmaker

load_dotenv()

import logging

# Configure logging
logging.basicConfig(level=logging.WARNING)
logger = logging.getLogger(__name__)
# Set the logger level explicitly
logger.setLevel(logging.WARNING)


def dtype_mapping():
    """Map Pandas data types to SQLAlchemy data types."""
    return {"object": String, "int64": Integer, "float64": Float, "datetime64[ns]": DateTime}


def update_or_create_new_table_from_df(df: pd.DataFrame, table_name: str, engine):
    # Create a new MetaData instance
    metadata = MetaData()

    # Identify the index column name
    index_name = df.index.name if df.index.name else "index"

    # Create or reflect the table
    if table_name in metadata.tables:
        existing_table = metadata.tables[table_name]
        logger.info(f"Using existing table: {table_name}")
    else:
        # Dynamically create the table schema based on the DataFrame's structure
        columns = [Column(index_name, String, primary_key=True)]  # Adjust the type as needed
        for col_name, col_type in df.dtypes.items():
            sqlalchemy_type = dtype_mapping().get(str(col_type), String)
            columns.append(Column(col_name, sqlalchemy_type))
        existing_table = Table(table_name, metadata, *columns)
        metadata.create_all(engine)  # Create the table in the database
        logger.info(f"Created new table: {table_name}")

    # Start a new session for each function call
    Session = sessionmaker(bind=engine)  # noqa: N806
    session = Session()

    try:
        df_reset = df.reset_index()
        data_dict = df_reset.to_dict("records")

        for record in data_dict:
            logger.debug(f"Processing record: {record}")
            existing_row = (
                session.query(existing_table)
                .filter(existing_table.c[index_name] == record[index_name])
                .first()
            )
            if existing_row:
                session.query(existing_table).filter(
                    existing_table.c[index_name] == record[index_name]
                ).update(record)
                logger.info(f"Updated record: {record[index_name]}")
            else:
                ins = existing_table.insert().values(**record)
                session.execute(ins)
                logger.info(f"Inserted record: {record[index_name]}")

        session.commit()  # Commit the session after processing all records
        logger.info("Transaction committed successfully.")
    except Exception as e:
        logger.error(f"Error inserting or updating data: {e}")  # noqa: TRY400
        session.rollback()
    finally:
        session.close()
        logger.info("Session closed.")


def drop_table(table_name: str, engine):
    metadata = MetaData()
    # Reflect the table you want to drop
    table_to_drop = Table(table_name, metadata, autoload_with=engine)

    # Drop the table
    table_to_drop.drop(engine)
    print(f"Table '{table_name}' dropped successfully!")
