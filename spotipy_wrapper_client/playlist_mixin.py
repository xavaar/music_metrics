from collections.abc import Iterable

import pandas as pd


class PlaylistMixin:
    def get_playlist_tracks_dataframe(self, playlists_info):
        # Your existing implementation
        data = []
        for playlist_info in playlists_info:
            playlist_id = playlist_info["id"]
            playlist = self.spotipy.user_playlist(self.user_id, playlist_id)
            playlist_name = playlist["name"]
            track_ids = [item["track"]["id"] for item in playlist["tracks"]["items"]]
            for track_id in track_ids:
                data.append(
                    {
                        "Playlist Name": playlist_name,
                        "Playlist ID": playlist_id,
                        "Track ID": track_id,
                    }
                )
        df = pd.DataFrame(data)
        return df
