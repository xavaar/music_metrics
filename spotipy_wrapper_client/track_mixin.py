from collections.abc import Iterable

import pandas as pd


class TrackMixin:
    def gettrackfeatures(self, ids: Iterable) -> list:
        """
        Fetches track metadata and audio features for a list of track IDs.

        :param ids: List of Spotify track IDs
        :return: List of dictionaries containing track information
        """
        tracks_info = []

        # Fetch metadata for multiple tracks
        tracks_meta = self.spotipy.tracks(ids)["tracks"]
        # Fetch audio features for multiple tracks
        tracks_features = self.spotipy.audio_features(ids)

        for meta, features in zip(tracks_meta, tracks_features):
            # Combine metadata and features
            track_info = features
            track_info["name"] = meta["name"]
            track_info["album"] = meta["album"]["name"]
            track_info["album_artists"] = "\x1f\x1e\x1d".join(
                [x["name"] for x in meta["album"]["artists"]]
            )  # It should display multiple artists for collabs but it doesn't
            track_info["artists"] = "\x1f\x1e\x1d".join(
                [x["name"] for x in meta["artists"]]
            )  # It should display multiple artists for collabs but it doesn't
            track_info["artist_uri"] = "\x1f\x1e\x1d".join([x["uri"] for x in meta["artists"]])
            track_info["release_date"] = meta["album"]["release_date"]
            track_info["popularity"] = meta["popularity"]
            track_info["track_number"] = meta["track_number"]

            tracks_info.append(track_info)

        return tracks_info
