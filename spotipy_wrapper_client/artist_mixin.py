import time
from collections.abc import Iterable

import pandas as pd


class ArtistMixin:
    def find_artist_by_name(self, artists, name):
        for artist in artists:
            if artist["name"] == name:
                return artist
        return None

    def get_artist_info(self, artist_names: Iterable):
        artist_info = {}

        for artist_name in artist_names:
            results = self.spotipy.search(q=artist_name, type="artist", limit=5)
            if results["artists"]["items"]:
                artist = self.find_artist_by_name(results["artists"]["items"], artist_name)
                if artist is not None:
                    artist_info[artist_name] = {
                        "URI": artist["uri"],
                        "Followers": artist["followers"]["total"],
                        "Genres": artist["genres"],
                        "Popularity": artist["popularity"],
                    }
                else:
                    print(f"Info for {artist_name} not found")

        # Convert the artist_info dictionary to a Pandas DataFrame
        df = pd.DataFrame.from_dict(artist_info, orient="index")
        df = df.reset_index()
        df.columns = ["artist_name", "artist_uri", "followers", "genres", "popularity"]
        df["genres"] = df["genres"].apply(lambda x: "\x1f\x1e\x1d".join(x))

        # Set the 'URI' column as the DataFrame index
        df = df.set_index("artist_uri")

        return df

    def get_artist_info_from_uri(self, artist_uris):
        artist_info = {}

        # Process in batches of 50
        for i in range(0, len(artist_uris), 50):
            batch = artist_uris[i : i + 50]  # Get next batch of artist URIs
            results = self.spotipy.artists(batch)  # Fetch information for the batch

            for result in results["artists"]:
                artist_info[result["uri"]] = {
                    "artist_uri": result["uri"],
                    "artist_name": result["name"],
                    "followers": result["followers"]["total"],
                    "genres": result["genres"],
                    "popularity": result["popularity"],
                    "spotify_info_last_updated": time.ctime(),
                }

        # Convert the artist_info dictionary to a Pandas DataFrame
        df = pd.DataFrame.from_dict(artist_info, orient="index")
        df.columns = [
            "artist_uri",
            "artist_name",
            "followers",
            "genres",
            "popularity",
            "spotify_info_last_updated",
        ]
        df["genres"] = df["genres"].apply(lambda x: "\x1f\x1e\x1d".join(x))

        # Set the 'URI' column as the DataFrame index
        df = df.set_index("artist_uri")

        return df
