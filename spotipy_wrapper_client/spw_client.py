import os
import time
from collections.abc import Iterable
from pathlib import Path

import numpy as np
import pandas as pd
import spotipy
from dotenv import load_dotenv
from spotipy.oauth2 import SpotifyOAuth

load_dotenv()

from artist_mixin import ArtistMixin
from playlist_mixin import PlaylistMixin
from track_mixin import TrackMixin


class SpwClient(PlaylistMixin, TrackMixin, ArtistMixin):
    def __init__(self):
        scope = (
            "user-read-private user-read-email user-read-playback-state user-modify-playback-state "
            "user-read-currently-playing user-library-modify user-library-read user-top-read "
            "user-read-recently-played user-follow-read user-follow-modify playlist-read-private "
            "playlist-modify-public playlist-modify-private playlist-read-collaborative streaming "
            "app-remote-control ugc-image-upload"
        )

        self.spotipy = spotipy.Spotify(auth_manager=SpotifyOAuth(scope=scope))
        self.user_id = self.spotipy.current_user()["id"]
        self.user_playlists = self.spotipy.user_playlists(self.user_id)
