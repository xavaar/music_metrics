#!/usr/bin/env bash
# This script should only be used in the context of a Gitlab pipeline
# The following invocation patterns are valid
# BUMP={major,minor,patch} ./bin/bump-version.sh // bumps version e.g. 1.0.0 => 2.0.0
# BUMP={major,minor,patch} DEV_RELEASE=true ./bin/bump-version.sh // create a dev release - bumps version and adds beta qualifier e.g. 1.0.0 => 2.0.0-beta1
# BUMP={build} DEV_RELEASE=true ./bin/bump-version.sh // increments build number e.g. 1.0.0-beta1 => 1.0.0-beta2
# PROMOTE_RELEASE=true ./bin/bump-version.sh // finalize dev release e.g. 1.0.0-beta2 => 1.0.0

set -e

REQUIRED_VARS=(
  GITLAB_USER_EMAIL
  CI_PROJECT_DIR
  CI_PROJECT_PATH
  CI_COMMIT_REF_NAME
  CI_SERVER_HOST
  CI_JOB_ID
  # BUMP (optional)
  # DEV_RELEASE (optional)
  # PROMOTE_RELEASE (optional)
)

for VAR_NAME in "${REQUIRED_VARS[@]}"; do
  VAL=${!VAR_NAME}
  if [[ -z $VAL ]]; then
    echo "$VAR_NAME must be set." >&2
    exit 1
  fi
done

# Configure git
# GITLAB_USER_EMAIL is a pre-defined variable - see https://docs.gitlab.com/ee/ci/variables/predefined_variables.html
git config user.email || git config --global user.email "$GITLAB_USER_EMAIL"
git config user.name  || git config --global user.name  "Gitlab Build $CI_JOB_ID"

# Checkout the branch
cd $CI_PROJECT_DIR

git checkout $CI_COMMIT_REF_NAME
git pull

# Get current version
search_string="version = "
line=$(grep -m 1 "$search_string" "pyproject.toml")
current_version=${line#*$search_string}

echo "BUMP: $BUMP"
echo "DEV_RELEASE: $DEV_RELEASE"
echo "PROMOTE_RELEASE: $PROMOTE_RELEASE"
echo "Current version: $current_version"

# Users of this script should only manipulate the release portion via DEV_RELEASE or PROMOTE_RELEASE
if [[ $BUMP == "release" ]]; then
    echo "ERROR: Manually incrementing the release portion of the version string is not supported. Please use DEV_RELEASE if you'd like to start a dev release or PROMOTE_RELEASE if you're ready to finalize a release."
    exit 1
# Don't allow build increments of stable builds
elif [[ -n "$DEV_RELEASE" && -z "$BUMP" ]]; then
    echo "ERROR: DEV_RELEASE is only valid when (1) starting a new dev release or (2) incrementing the build number of a dev release"
    exit 1
# PROMOTE_RELEASE should be defined in isolation when a beta version is already present
elif [[ -n "$PROMOTE_RELEASE" && -n "$BUMP" ]]; then
    echo "ERROR: PROMOTE_RELEASE is only valid when finalizing a dev release."
    exit 1
elif [[ -n "$PROMOTE_RELEASE" && -z "$BUMP" && ! "$current_version" =~ beta ]]; then
    echo "ERROR: PROMOTE_RELEASE is only valid when finalizing a dev release."
    exit 1
# Don't allow build increments of stable builds
elif [[ $BUMP == "build" && ! "$current_version" =~ beta ]]; then
    echo "ERROR: You are trying to increment the build number of a stable build. First make a dev release."
    exit 1
# Don't allow build increments of stable builds
elif [[ $BUMP == "build" && -z "$DEV_RELEASE" ]]; then
    echo "ERROR: You must specify DEV_RELEASE=true when incremental the build number."
    exit 1
fi

# If BUMP and DEV_RELEASE are defined this means the user is either:
# (1) Trying to increment a main/minor/patch number and add the beta qualifier or
# (2) Trying to increment the build number when the beta qualifier is already present
# In either case only a single command is required
if [[ -n "$BUMP" && -n "$DEV_RELEASE" ]]; then
    bumpversion --verbose $BUMP # e.g. 1.0.0 -> 1.0.1-beta1 OR 1.0.1-beta1 => 1.0.1-beta2

# If BUMP is defined without DEV_RELEASE, the user is solely trying to increment
# the version number, i.e. go from 1.0.0 -> 1.0.1, this actually requires two
# commands
elif [[ -n "$BUMP" && -z "$DEV_RELEASE" ]]; then
    bumpversion --verbose --no-tag --no-commit $BUMP # e.g. 1.0.0 -> 1.0.1-beta1
    bumpversion --verbose --allow-dirty release # e.g. 1.0.0-beta1 -> 1.0.1 (1.0.1-stable1)

# If BUMP is not defined and PROMOTE_RELEASE is,  the user is trying to finalize a release,
# i.e. go from beta -> stable
elif [[ -z "$BUMP" && -n "$PROMOTE_RELEASE" ]]; then
  bumpversion --verbose release; # e.g. 1.0.0-beta1 -> 1.0.0 (1.0.0-stable1)
else
  echo "ERROR: You must specify BUMP, DEV_RELEASE, and/or PROMOTE_RELEASE - see script for correct usage"
  exit 1
fi

# Push the changes
git remote set-url --push origin "git@${CI_SERVER_HOST}:${CI_PROJECT_PATH}.git"
git push --tags origin $CI_COMMIT_REF_NAME
