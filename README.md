## Known Issues

Genius IDs sometimes map to multiple artists with the same name.
Genius IDs require an exact match, for some artist partiuclarly Korean and Latin artists the Genius name has characters that don't match the spotify name.


# Motive

This project is an attempt to answer a facet of a much larger question.

What creates culture?

More specfically how can we measure and visualize cultural vibrancy, particularly in cities. How does culture form, grow, and propogate? What factors influence this process?

I chose music for this project because there's a realtively large amount of information available relative to other cultural activities such as architecture, food, or the visual arts.

In the grander scheme I believe that we are in a time of great cultural inequality. What does this mean? Much like how most wealth is held by held by a relatively few individuals I believe that culture is being dispropotionaly produced in a few geographic areas relative to previous eras of history. This is likely tied to economic inequality as metropolises such as LA and New York dominate both economic and culutral activity.

Overall I believe this dynamic is making our society weeker and less vibrant. As a few localities absorb cultural producers from across the country the rest of the nation is left destitute, a monotonous expanse corporate franchises and mass produced goods. This is particulary apparent in once thriving cities in the rust belt like Detroit or Cleveland. Undoutably these trend is tied to the economic fortunes of this region.

Looking forward I pose the question, what can we do to restore vitality to cities across the nation? I beilieve the process is rooted in infrastructure and design.
